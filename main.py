import random
import os
import time

plateau = [
["","A","B","C","D","E","F","G","H","I","J"," "," "],
["╔","═","═","═","═","═","═","═","═","═","═","╗"," "],
["║"," "," "," "," "," "," "," "," "," "," ","║","0"],
["║"," "," "," "," "," "," "," "," "," "," ","║","1"],
["║"," "," "," "," "," "," "," "," "," "," ","║","2"],
["║"," "," "," "," "," "," "," "," "," "," ","║","3"],
["║"," "," "," "," "," "," "," "," "," "," ","║","4"],
["║"," "," "," "," "," "," "," "," "," "," ","║","5"],
["║"," "," "," "," "," "," "," "," "," "," ","║","6"],
["║"," "," "," "," "," "," "," "," "," "," ","║","7"],
["║"," "," "," "," "," "," "," "," "," "," ","║","8"],
["║"," "," "," "," "," "," "," "," "," "," ","║","9"],
["╚","═","═","═","═","═","═","═","═","═","═","╝"," "],
[" "," "," "," "," "," "," "," "," "," "," "," "," "],
]

def PlateauAlaatoire(nbnoirs,nbblancs):
  
    count_noir = 0
    count_blanc = 0
    
    while (count_noir < nbnoirs) | (count_blanc < nbblancs):

        random_case = random.randint(1,10)
        random_line = random.randint(2,11)

        #Si ligne du plateau est impaire on veut que la case soit impaire
        if (random_line % 2) == 1:    

            #Si case du plateau est impaire mettre "n" ou "b" dans " "
            if (random_case % 2) == 1:

                if (plateau[random_line][random_case] == " ") & (count_noir < nbnoirs):
                    plateau[random_line][random_case] = "n"
                    count_noir += 1

                elif (plateau[random_line][random_case] == " "):
                    plateau[random_line][random_case] = "b"
                    count_blanc += 1
        
        #Sinon ligne du plateau est paire on veut que la case soit paire
        else:
            #Si case du plateau est paire mettre "n" ou "b" dans " "
            if (random_case % 2) == 0:

                if (plateau[random_line][random_case] == " ") & (count_noir < nbnoirs):
                    plateau[random_line][random_case] = "n"
                    count_noir += 1

                if (plateau[random_line][random_case] == " ") :
                    plateau[random_line][random_case] = "b"
                    count_blanc += 1

def NombreDePion():
    nb_noir = 0
    nb_blanc = 0

    for i in range(2,12):
        count = 1

        while count < 11:

            if plateau[i][count] == "n":
                nb_noir += 1

            if plateau[i][count] == "b":
                nb_blanc += 1

            count += 1

    #permettre la fin du jeu lorsque le joueur arrive à 0 point avec la fct FinDeJeu()
    if nb_noir == 0:
        return 1

    elif nb_blanc == 0:
        return 0

    #permet l'affichage des scores
    else:
        AfficheScore(nb_noir,nb_blanc)

def AffichePlateau():
    line = ""
    liste_case = ["[","]",", "," ","'","n","b","A","B","C","D","E","F","G","H","I","J","0","1","2","3","4","5","6","7","8","9","═","r","l"]
    liste_case2 = ["","","","   ",""," ○ "," ● ","  A "," B "," C "," D "," E "," F "," G "," H "," I "," J "," 0"," 1"," 2"," 3"," 4"," 5"," 6"," 7"," 8"," 9","═══"," ☻ "," ☺ "]
    
    for ligne in range(2,12):

        for case in range(1,11):

            if (ligne % 2) == 1:

                if (case % 2) == 0:
                    plateau[ligne][case] = "███"

            else:

                if (case % 2) == 1:
                    plateau[ligne][case] = "███"

    for i in range(0,13):
        line = str(plateau[i])
        count_case = 0
        
        while count_case < len(liste_case):
            line = line.replace(liste_case[count_case],liste_case2[count_case])
            count_case += 1
        print(line)

    return None

def TrouverPionsBlancsJouables():
    pions_blancs_jouables = []
    pions_blancs_capturables_blancs = []

    for i in range(2,12):
        count_case = 1

        while count_case < 11:

            if plateau[i][count_case] == "b":
                
                #cherche les pions blancs jouables vers le haut dans des cases vide
                if (plateau[i - 1][count_case - 1] == " ") | (plateau[i - 1][count_case + 1] == " "):
                    pions_blancs_jouables.append(str(plateau[0][count_case] + plateau[i][12]))

                #cherche les pions blancs jouables avec une capture de pion noir suite a une case vide derrière
                if ( (plateau[i - 1][count_case - 1] == "n") & (plateau[i - 2][count_case - 2] == " ") ) | ( (plateau[i - 1][count_case + 1] == "n") & (plateau[i - 2][count_case + 2] == " ") ) | ( (plateau[i + 1][count_case - 1] == "n") & (plateau[i + 2][count_case - 2] == " ") ) | ( (plateau[i + 1][count_case + 1] == "n") & (plateau[i + 2][count_case + 2] == " ") ):
                    pions_blancs_capturables_blancs.append(str(plateau[0][count_case] + plateau[i][12]))

            count_case += 1

    #si la liste des pions capturables est vide on return la liste des pions jouables pour le déplacement
    if pions_blancs_capturables_blancs == []:
        print("Le joueur blanc peut bouger les pions sur les cases : " + str(pions_blancs_jouables))
        return pions_blancs_jouables

    else:
        print("Le joueur blanc peut bouger les pions sur les cases pour capturer les pions noirs : " + str(pions_blancs_capturables_blancs))
        return pions_blancs_capturables_blancs

def TrouverPionsNoirsJouables():
    pions_noirs_jouables = []
    pions_blancs_capturables_noirs = []

    for i in range(2,12):
        count_case = 1

        while count_case < 11:

            if plateau[i][count_case] == "n":
                
                #cherche les pions noirs jouables vers le haut dans des cases vide
                if (plateau[i + 1][count_case - 1] == " ") | (plateau[i + 1][count_case + 1] == " "):
                        pions_noirs_jouables.append(str(plateau[0][count_case] + plateau[i][12]))
                
                #cherche les pions noirs jouables avec une capture de pion blanc suite a une case vide derrière
                if ( (plateau[i - 1][count_case - 1] == "b") & (plateau[i - 2][count_case - 2] == " ") ) | ( (plateau[i - 1][count_case + 1] == "b") & (plateau[i - 2][count_case + 2] == " ") ) | ( (plateau[i + 1][count_case - 1] == "b") & (plateau[i + 2][count_case - 2] == " ") ) | ( (plateau[i + 1][count_case + 1] == "b") & (plateau[i + 2][count_case + 2] == " ") ):
                    pions_blancs_capturables_noirs.append(str(plateau[0][count_case] + plateau[i][12]))

            count_case += 1

    #si la liste des pions capturables est vide on return la liste des pions jouables pour le déplacement
    if pions_blancs_capturables_noirs == []:
        print("Le joueur noir peut bouger les pions sur les cases : " + str(pions_noirs_jouables))
        return pions_noirs_jouables

    else:
        print("Le joueur noir peut bouger les pions sur les cases pour capturer les pions noirs : " + str(pions_blancs_capturables_noirs))
        return pions_blancs_capturables_noirs

def DeplacerPionBlanc(lettre_depart,chiffre_depart,position_depart):
    time.sleep(2)
    os.system('cls' if os.name == 'nt' else 'clear')

    lettre_arriver = ""
    chiffre_arriver = ""
    
    AffichePlateau()

    print("Le joueur blanc a choisit le pion : " + str(position_depart))
    print(end="\n")

    #demande au joueur ou veut il déplacer son pion 
    while (lettre_arriver != "A") & (lettre_arriver != "B") & (lettre_arriver != "C") & (lettre_arriver != "D") & (lettre_arriver != "E") & (lettre_arriver != "F") & (lettre_arriver != "G") & (lettre_arriver != "H") & (lettre_arriver != "I") & (lettre_arriver != "J"):
        lettre_arriver = input("Entrez une lettre entre A et J pour saisir la colonne de la case d'arriver du pion: ")

    while (chiffre_arriver != "1") & (chiffre_arriver != "2") & (chiffre_arriver != "3") & (chiffre_arriver != "4") & (chiffre_arriver != "5") & (chiffre_arriver != "6") & (chiffre_arriver != "7") & (chiffre_arriver != "8") & (chiffre_arriver != "9") & (chiffre_arriver != "0"):
        chiffre_arriver = input("Entrez un chiffre entre 0 et 9 pour saisir la ligne de la case d'arriver du pion : ")
        print(end="\n")

    #passage de la lettre et du chiffre entrer par le joueur en chiffre grace au plateau 
    for i in range(1,12):

        if plateau[0][i] == lettre_depart:
            colonne_depart = i 
                
        if plateau[i][12] == chiffre_depart:
            ligne_depart = i  

        if  plateau[0][i] == lettre_arriver:
            colonne_arriver = i

        if plateau[i][12] == chiffre_arriver:
            ligne_arriver = i 
    
    position_depart = lettre_depart + str(chiffre_depart)
    position_arriver = lettre_arriver + str(chiffre_arriver)
    
    print("Le joueur blanc veut deplacer le pion à la position : " + str(position_arriver))
    print(end="\n")

    #permet le déplacement des dames si le pion choisi est une dame
    if plateau[ligne_depart][colonne_depart] == "r":
        DeplacementDame(ligne_depart,colonne_depart,ligne_arriver,colonne_arriver)

    else:
        #permet le déplacement d'un pion pour le capturer 
        if ( ((plateau[ligne_depart - 1][colonne_depart - 1]  == "n") & (plateau[ligne_depart - 2][colonne_depart - 2]  == " ")) | ((plateau[ligne_depart - 1][colonne_depart + 1]  == "n") & (plateau[ligne_depart - 2][colonne_depart + 2]  == " ")) | ((plateau[ligne_depart + 1][colonne_depart - 1]  == "n") & (plateau[ligne_depart + 2][colonne_depart - 2]  == " ")) | ((plateau[ligne_depart + 1][colonne_depart + 1]  == "n") & (plateau[ligne_depart + 2][colonne_depart + 2]  == " ")) ):
            
            #fct qui permet de capturer un poin abverse
            DeplacerPionCapturableBlanc(ligne_arriver,ligne_depart,colonne_arriver,colonne_depart,position_arriver,lettre_depart,chiffre_depart,position_depart)
            #permet de faire plusieur capture si possible
            NouvelleCaptureDePionBlanc(ligne_arriver,colonne_arriver,position_arriver,lettre_arriver,chiffre_arriver)
        #permet un simple déplacement    
        else:
            #permettre de redemander une autre position si les casse en diagonales du pion choisi ne pas respecter
            if ( (ligne_arriver == (ligne_depart - 1)) & (colonne_arriver == (colonne_depart - 1)) ) | ( (ligne_arriver == (ligne_depart - 1)) & (colonne_arriver == (colonne_depart + 1)) ):
                
                #permettre de redemander une autre position si les casse en diagonales du pion choisi ne pas vide
                if (plateau[ligne_depart][colonne_depart] == "b") & (plateau[ligne_arriver][colonne_arriver] == " "):
                    plateau[ligne_arriver][colonne_arriver] = "b"
                    plateau[ligne_depart][colonne_depart] = " "

                    AffichePlateau()

                    print("le pion blanc en " + str(position_depart), "a été déplacer en " + str(position_arriver) )
                    print(end="\n") 

                else:
                    print("la case " + str(position_arriver), "n'est pas vide !!!")
                    print(end="\n") 
                    DeplacerPionBlanc(lettre_depart,chiffre_depart,position_depart) 

            else:
                print("Vous ne pouvez pas mettre votre pion dans la case " + str(position_arriver))
                print(end="\n") 
                DeplacerPionBlanc(lettre_depart,chiffre_depart,position_depart)
    
    return 1

def PartieBlanc(pion_noir,pion_blanc):
    time.sleep(2)
    os.system('cls' if os.name == 'nt' else 'clear')
    Dame()
    AffichePlateau()    

    print("Au Joueur Blanc de jouer")
    print(end="\n")

    position = ""
    lettre_depart = ""
    chiffre_depart = ""
    abandon = ""
    result = 0

    print("Voulez-vous abandonner la partie")
    abandon = input("Taper (OUI/NON) ")
    print(end="\n")

    if abandon == "OUI":
        abandon = 2
        FinDeJeu(abandon)

    elif abandon == "NON":
        position_jouable = TrouverPionsBlancsJouables()
        dame_jouable = TrouverDameBlanc()
        
        #demande au joueur quel pion veut il déplacer
        while (lettre_depart != "A") & (lettre_depart != "B") & (lettre_depart != "C") & (lettre_depart != "D") & (lettre_depart != "E") & (lettre_depart != "F") & (lettre_depart != "G") & (lettre_depart != "H") & (lettre_depart != "I") & (lettre_depart != "J"):
                lettre_depart = input("Entrez une lettre entre A et J pour saisir la colonne du pion que vous voulez deplacer : ")

        while (chiffre_depart != "1") & (chiffre_depart != "2") & (chiffre_depart != "3") & (chiffre_depart != "4") & (chiffre_depart != "5") & (chiffre_depart != "6") & (chiffre_depart != "7") & (chiffre_depart != "8") & (chiffre_depart != "9") & (chiffre_depart != "0"):
            chiffre_depart = input("Entrez un chiffre entre 0 et 9 pour saisir la ligne du poin que vous voulez deplacer : ")
            print(end="\n")

        position = lettre_depart + str(chiffre_depart)

        if dame_jouable != None:
            #permet de jouer avec la dame
            for i in range(0,len(dame_jouable)):

                if position == dame_jouable[i]:
                    result = DeplacerPionBlanc(lettre_depart,chiffre_depart,position)

                else:
                    #si la position qu'on a choisi est dans la liste on lance le deplacement
                    for i in range(0,len(position_jouable)):

                        if (position == position_jouable[i]):
                            result = DeplacerPionBlanc(lettre_depart,chiffre_depart,position)
        else:
            #si la position qu'on a choisi est dans la liste on lance le deplacement
            for i in range(0,len(position_jouable)):

                if (position == position_jouable[i]):
                    result = DeplacerPionBlanc(lettre_depart,chiffre_depart,position)
       
        #permet de lancer la partie du joueur noir grace au return de la fct DeplacerPionBlanc()
        if result:
            NombreDePion()
            PartieNoir(pion_noir,pion_blanc)

        else:
            PartieBlanc(pion_noir,pion_blanc)

    else:
        PartieBlanc(pion_noir,pion_blanc)

def DeplacerPionNoir(lettre_depart,chiffre_depart,position_depart):
    time.sleep(2)
    os.system('cls' if os.name == 'nt' else 'clear')

    lettre_arriver = ""
    chiffre_arriver = ""
    
    AffichePlateau()

    print("Le joueur noir a choisit le pion : " + str(position_depart))
    print(end="\n")

    #demande au joueur ou veut il déplacer son pion
    while (lettre_arriver != "A") & (lettre_arriver != "B") & (lettre_arriver != "C") & (lettre_arriver != "D") & (lettre_arriver != "E") & (lettre_arriver != "F") & (lettre_arriver != "G") & (lettre_arriver != "H") & (lettre_arriver != "I") & (lettre_arriver != "J"):
        lettre_arriver = input("Entrez une lettre entre A et J pour saisir la colonne de la case d'arriver du pion: ")

    while (chiffre_arriver != "1") & (chiffre_arriver != "2") & (chiffre_arriver != "3") & (chiffre_arriver != "4") & (chiffre_arriver != "5") & (chiffre_arriver != "6") & (chiffre_arriver != "7") & (chiffre_arriver != "8") & (chiffre_arriver != "9") & (chiffre_arriver != "0"):
        chiffre_arriver = input("Entrez un chiffre entre 0 et 9 pour saisir la ligne de la case d'arriver du pion : ")
        print(end="\n")

    #passage de la lettre et du chiffre entrer par le joueur en chiffre grace au plateau pour ça position
    for i in range(1,12):

        if plateau[0][i] == lettre_depart:
            colonne_depart = i 
                
        if plateau[i][12] == chiffre_depart:
            ligne_depart = i  

        if  plateau[0][i] == lettre_arriver:
            colonne_arriver = i

        if plateau[i][12] == chiffre_arriver:
            ligne_arriver = i 
    
    position_depart = lettre_depart + str(chiffre_depart)
    position_arriver = lettre_arriver + str(chiffre_arriver)
    
    print("Le joueur noir veut deplacer le pion à la pisition : " + str(position_arriver))
    print(end="\n")

    #permet le déplacement des dames si le pion choisi est une dame
    if plateau[ligne_depart][colonne_depart] == "l":
        DeplacementDame(ligne_depart,colonne_depart,ligne_arriver,colonne_arriver)

    else:
        #permet le déplacement d'un pion pour le capturer 
        if ( ((plateau[ligne_depart - 1][colonne_depart - 1]  == "b") & (plateau[ligne_depart - 2][colonne_depart - 2]  == " ")) | ((plateau[ligne_depart - 1][colonne_depart + 1]  == "b") & (plateau[ligne_depart - 2][colonne_depart + 2]  == " ")) | ((plateau[ligne_depart + 1][colonne_depart - 1]  == "b") & (plateau[ligne_depart + 2][colonne_depart - 2]  == " ")) | ((plateau[ligne_depart + 1][colonne_depart + 1]  == "b") & (plateau[ligne_depart + 2][colonne_depart + 2]  == " ")) ): 
            #fct qui permet de capturer un poin abverse
            DeplacerPionCapturableNoir(ligne_arriver,ligne_depart,colonne_arriver,colonne_depart,position_arriver,lettre_depart,chiffre_depart,position_depart)
            #permet de faire plusieur capture si possible
            NouvelleCaptureDePionNoir(ligne_arriver,colonne_arriver,position_arriver,lettre_arriver,chiffre_arriver)

        #permet un simple déplacement
        else:
            #permettre de redemander une autre position si les casse en diagonales du pion choisi ne pas respecter
            if ( (ligne_arriver == (ligne_depart + 1)) & (colonne_arriver == (colonne_depart - 1)) ) | ( (ligne_arriver == (ligne_depart + 1)) & (colonne_arriver == (colonne_depart + 1)) ):
                
                #permettre de redemander une autre position si les casse en diagonales du pion choisi ne pas vide
                if (plateau[ligne_depart][colonne_depart] == "n") & (plateau[ligne_arriver][colonne_arriver] == " "):
                    plateau[ligne_arriver][colonne_arriver] = "n"
                    plateau[ligne_depart][colonne_depart] = " "

                    AffichePlateau()

                    print("le pion noir en " + str(position_depart), "a été déplacer en " + str(position_arriver) )
                    print(end="\n") 

                else:
                    print("la case " + str(position_arriver), "n'est pas vide !!!")
                    print(end="\n") 
                    DeplacerPionNoir(lettre_depart,chiffre_depart,position_depart) 

            else:
                print("Vous ne pouvez pas mettre votre pion dans la case " + str(position_arriver))
                print(end="\n") 
                DeplacerPionNoir(lettre_depart,chiffre_depart,position_depart)

    return 1

def PartieNoir(pion_noir,pion_blanc):
    time.sleep(2)
    os.system('cls' if os.name == 'nt' else 'clear')
    Dame()
    AffichePlateau()

    print("Au Joueur Noir de jouer")
    print(end="\n")

    position = ""
    lettre_depart = ""
    chiffre_depart = ""
    abandon = ""
    result = 0

    print("Voulez-vous abandonner la partie")
    abandon = input("Taper (OUI/NON) ")
    print(end="\n")

    if abandon == "OUI":
        abandon = 3
        FinDeJeu(abandon)

    elif abandon == "NON":  
        position_jouable = TrouverPionsNoirsJouables()
        dame_jouable = TrouverDameNoir() 

        #demande au joueur quel pion veut il déplacer
        while (lettre_depart != "A") & (lettre_depart != "B") & (lettre_depart != "C") & (lettre_depart != "D") & (lettre_depart != "E") & (lettre_depart != "F") & (lettre_depart != "G") & (lettre_depart != "H") & (lettre_depart != "I") & (lettre_depart != "J"):
                lettre_depart = input("Entrez une lettre entre A et J pour saisir la colonne du pion que vous voulez deplacer : ")
                
        while (chiffre_depart != "1") & (chiffre_depart != "2") & (chiffre_depart != "3") & (chiffre_depart != "4") & (chiffre_depart != "5") & (chiffre_depart != "6") & (chiffre_depart != "7") & (chiffre_depart != "8") & (chiffre_depart != "9") & (chiffre_depart != "0"):
            chiffre_depart = input("Entrez un chiffre entre 0 et 9 pour saisir la ligne du poin que vous voulez deplacer : ")
            print(end="\n")

        position = lettre_depart + str(chiffre_depart)

        
        if dame_jouable != None:
            #permet de jouer avec la dame
            for i in range(0,len(dame_jouable)):

                if position == dame_jouable[i]:
                 result = DeplacerPionBlanc(lettre_depart,chiffre_depart,position)

                else:
                    #si la position qu'on a choisi est dans la liste on lance le deplacement
                    for i in range(0,len(position_jouable)):

                        if position == position_jouable[i]:
                            result = DeplacerPionNoir(lettre_depart,chiffre_depart,position)

        else:
            #si la position qu'on a choisi est dans la liste on lance le deplacement
            for i in range(0,len(position_jouable)):
                
                if position == position_jouable[i]:
                    result = DeplacerPionNoir(lettre_depart,chiffre_depart,position)

        #permet de lancer la partie du joueur blanc grace au return de la fct DeplacerPionNoir()
        if result:
            NombreDePion()
            PartieBlanc(pion_noir,pion_blanc)
            
        else:
            PartieNoir(pion_noir,pion_blanc)

    else:
        PartieNoir(pion_noir,pion_blanc)

def DeplacerPionCapturableBlanc(ligne_arriver,ligne_depart,colonne_arriver,colonne_depart,position_arriver,lettre_depart,chiffre_depart,position_depart):
    if ( (ligne_arriver == (ligne_depart - 2)) & (colonne_arriver == (colonne_depart - 2)) ):

        if (plateau[ligne_depart][colonne_depart] == "b") & (plateau[ligne_depart - 1][colonne_depart - 1] == "n") & (plateau[ligne_arriver ][colonne_arriver ] == " "):
            plateau[ligne_arriver][colonne_arriver] = "b"
            plateau[ligne_depart - 1][colonne_depart - 1] = " "
            plateau[ligne_depart][colonne_depart] = " "
                
            AffichePlateau()

            print("Le joueur Blanc a pris le pion noir situé en " + str(str(plateau[0][colonne_depart - 1]) + str(plateau[ligne_depart - 1][12])) )
            print(end="\n") 

        else:
            print("Vous ne pouvez pas mettre votre pion dans la case " + str(position_arriver))
            print(end="\n") 
            DeplacerPionBlanc(lettre_depart,chiffre_depart,position_depart)
           
    elif ( (ligne_arriver == (ligne_depart - 2)) & (colonne_arriver == (colonne_depart + 2)) ):

        if (plateau[ligne_depart][colonne_depart] == "b") & (plateau[ligne_depart - 1][colonne_depart + 1] == "n") & (plateau[ligne_arriver ][colonne_arriver ] == " "):
            plateau[ligne_arriver][colonne_arriver] = "b"
            plateau[ligne_depart - 1][colonne_depart + 1] = " "
            plateau[ligne_depart][colonne_depart] = " "
                
            AffichePlateau()

            print("Le joueur Blanc a pris le pion noir situé en " + str(str(plateau[0][colonne_depart + 1]) + str(plateau[ligne_depart - 1][12])) )
            print(end="\n") 
                
        else:
            print("Vous ne pouvez pas mettre votre pion dans la case " + str(position_arriver))
            print(end="\n") 
            DeplacerPionBlanc(lettre_depart,chiffre_depart,position_depart)
            
    elif ( (ligne_arriver == (ligne_depart + 2)) & (colonne_arriver == (colonne_depart - 2)) ): 

        if (plateau[ligne_depart][colonne_depart] == "b") & (plateau[ligne_depart + 1][colonne_depart - 1] == "n") & (plateau[ligne_arriver ][colonne_arriver ] == " "):
            plateau[ligne_arriver][colonne_arriver] = "b"
            plateau[ligne_depart + 1][colonne_depart - 1] = " "
            plateau[ligne_depart][colonne_depart] = " "
                
            AffichePlateau()

            print("Le joueur Blanc a pris le pion noir situé en " + str(str(plateau[0][colonne_depart - 1]) + str(plateau[ligne_depart + 1][12])) )
            print(end="\n") 

        else:
            print("Vous ne pouvez pas mettre votre pion dans la case " + str(position_arriver))
            print(end="\n") 
            DeplacerPionBlanc(lettre_depart,chiffre_depart,position_depart)

    elif ( (ligne_arriver == (ligne_depart + 2)) & (colonne_arriver == (colonne_depart + 2)) ): 

        if (plateau[ligne_depart][colonne_depart] == "b") & (plateau[ligne_depart + 1][colonne_depart + 1] == "n") & (plateau[ligne_arriver ][colonne_arriver ] == " "):
            plateau[ligne_arriver][colonne_arriver] = "b"
            plateau[ligne_depart + 1][colonne_depart + 1] = " "
            plateau[ligne_depart][colonne_depart] = " "
            
            AffichePlateau()

            print("Le joueur Blanc a pris le pion noir situé en " + str(str(plateau[0][colonne_depart + 1]) + str(plateau[ligne_depart + 1][12])) )
            print(end="\n") 

        else:
            print("Vous ne pouvez pas mettre votre pion dans la case " + str(position_arriver))
            print(end="\n") 
            DeplacerPionBlanc(lettre_depart,chiffre_depart,position_depart)

    else:
            print("Vous ne pouvez pas mettre votre pion dans la case " + str(position_arriver))
            print(end="\n") 
            DeplacerPionBlanc(lettre_depart,chiffre_depart,position_depart)

def DeplacerPionCapturableNoir(ligne_arriver,ligne_depart,colonne_arriver,colonne_depart,position_arriver,lettre_depart,chiffre_depart,position_depart):
    if ( (ligne_arriver == (ligne_depart - 2)) & (colonne_arriver == (colonne_depart - 2)) ):

        if (plateau[ligne_depart][colonne_depart] == "n") & (plateau[ligne_depart - 1][colonne_depart - 1] == "b") & (plateau[ligne_arriver ][colonne_arriver ] == " "):
            plateau[ligne_arriver][colonne_arriver] = "n"
            plateau[ligne_depart - 1][colonne_depart - 1] = " "
            plateau[ligne_depart][colonne_depart] = " "
                
            AffichePlateau()

            print("Le joueur noir a pris le pion blanc situé en " + str(str(plateau[0][colonne_depart - 1]) + str(plateau[ligne_depart - 1][12])) )
            print(end="\n") 

        else:
            print("Vous ne pouvez pas mettre votre pion dans la case " + str(position_arriver))
            print(end="\n") 
            DeplacerPionNoir(lettre_depart,chiffre_depart,position_depart)
           
    elif ( (ligne_arriver == (ligne_depart - 2)) & (colonne_arriver == (colonne_depart + 2)) ):

        if (plateau[ligne_depart][colonne_depart] == "n") & (plateau[ligne_depart - 1][colonne_depart + 1] == "b") & (plateau[ligne_arriver ][colonne_arriver ] == " "):
            plateau[ligne_arriver][colonne_arriver] = "n"
            plateau[ligne_depart - 1][colonne_depart + 1] = " "
            plateau[ligne_depart][colonne_depart] = " "
                
            AffichePlateau()

            print("Le joueur noir a pris le pion blanc situé en " + str(str(plateau[0][colonne_depart + 1]) + str(plateau[ligne_depart - 1][12])) )
            print(end="\n") 
                
        else:
            print("Vous ne pouvez pas mettre votre pion dans la case " + str(position_arriver))
            print(end="\n") 
            DeplacerPionNoir(lettre_depart,chiffre_depart,position_depart)
            
    elif ( (ligne_arriver == (ligne_depart + 2)) & (colonne_arriver == (colonne_depart - 2)) ): 

        if (plateau[ligne_depart][colonne_depart] == "n") & (plateau[ligne_depart + 1][colonne_depart - 1] == "b") & (plateau[ligne_arriver ][colonne_arriver ] == " "):
            plateau[ligne_arriver][colonne_arriver] = "n"
            plateau[ligne_depart + 1][colonne_depart - 1] = " "
            plateau[ligne_depart][colonne_depart] = " "
                
            AffichePlateau()

            print("Le joueur noir a pris le pion blanc situé en " + str(str(plateau[0][colonne_depart - 1]) + str(plateau[ligne_depart + 1][12])) )
            print(end="\n") 

        else:
            print("Vous ne pouvez pas mettre votre pion dans la case " + str(position_arriver))
            print(end="\n") 
            DeplacerPionNoir(lettre_depart,chiffre_depart,position_depart)

    elif ( (ligne_arriver == (ligne_depart + 2)) & (colonne_arriver == (colonne_depart + 2)) ): 

        if (plateau[ligne_depart][colonne_depart] == "n") & (plateau[ligne_depart + 1][colonne_depart + 1] == "b") & (plateau[ligne_arriver ][colonne_arriver ] == " "):
            plateau[ligne_arriver][colonne_arriver] = "n"
            plateau[ligne_depart + 1][colonne_depart + 1] = " "
            plateau[ligne_depart][colonne_depart] = " "
                
            AffichePlateau()

            print("Le joueur noir a pris le pion blanc situé en " + str(str(plateau[0][colonne_depart + 1]) + str(plateau[ligne_depart + 1][12])) )
            print(end="\n") 

        else:
            print("Vous ne pouvez pas mettre votre pion dans la case " + str(position_arriver))
            print(end="\n") 
            DeplacerPionNoir(lettre_depart,chiffre_depart,position_depart)

    else:
            print("Vous ne pouvez pas mettre votre pion dans la case " + str(position_arriver))
            print(end="\n") 
            DeplacerPionNoir(lettre_depart,chiffre_depart,position_depart)

def AfficheScore(sore_noir,score_blanc):
    plateau_score = [
    ["╔════════════════════╗"],
    ["║","Joueur Blanc",":",str(score_blanc)," ","║"],
    ["║","Joueur Noir ",":",str(sore_noir)," ","║"],
    ["╚════════════════════╝"],
    ]
    
    liste_case = ["[","]",", "," ","'",":"]
    liste_case2 = ["","",""," ",""," :   "]

    for i in range(0,4):
        line = str(plateau_score[i])
        count_case = 0
        
        while count_case < len(liste_case):
            line = line.replace(liste_case[count_case],liste_case2[count_case])
            count_case += 1

        print(line)

    return None

def FinDeJeu(abandon):
    if abandon == 2:
        print("Le joueur Blanc a abandonné la partie ! Bien joué au joueur Noir.")

    elif abandon == 3:
        print("Le joueur Noir a abandonné la partie ! Bien joué au joueur Blanc.")

    if not((abandon == 2) | (abandon == 2)):
        result = NombreDePion()

        if result == 1:
            print("Le joueur Noir remporte la partie ! Bien joué au joueur Blanc.")

        elif result == 0:
            print("Le joueur Blanc remporte la partie ! Bien joué au joueur Noir.")

def InitialisationDesPions():
    for line in range(2,len(plateau)):

        for case in range(1,12):
                    
            if ((line % 2) == 1) & ((case % 2) == 1):    

                if (plateau[line][case] == " ") & (line < 6):
                    plateau[line][case] = "n"

                elif (plateau[line][case] == " ") & (line >= 8):
                    plateau[line][case] = "b"

            else:
               
                if (plateau[line][case] == " ") & (line < 6):
                    plateau[line][case] = "n"
                    
                elif (plateau[line][case] == " ") & ( line >= 8):
                    plateau[line][case] = "b"

def NouvelleCaptureDePionBlanc(ligne_arriver,colonne_arriver,position_arriver,lettre_arriver,chiffre_arriver):
    if ( ((plateau[ligne_arriver - 1][colonne_arriver - 1]  == "n") & (plateau[ligne_arriver - 2][colonne_arriver - 2]  == " ")) | ((plateau[ligne_arriver - 1][colonne_arriver + 1]  == "n") & (plateau[ligne_arriver - 2][colonne_arriver + 2]  == " ")) | ((plateau[ligne_arriver + 1][colonne_arriver - 1]  == "n") & (plateau[ligne_arriver + 2][colonne_arriver - 2]  == " ")) | ((plateau[ligne_arriver + 1][colonne_arriver + 1]  == "n") & (plateau[ligne_arriver + 2][colonne_arriver + 2]  == " ")) ):
        #relance la fct DeplacerPionBlanc avec la position d'arriver du pion Blanc
        DeplacerPionBlanc(lettre_arriver,chiffre_arriver,position_arriver)

def NouvelleCaptureDePionNoir(ligne_arriver,colonne_arriver,position_arriver,lettre_arriver,chiffre_arriver):
    if ( ((plateau[ligne_arriver - 1][colonne_arriver - 1]  == "b") & (plateau[ligne_arriver - 2][colonne_arriver - 2]  == " ")) | ((plateau[ligne_arriver - 1][colonne_arriver + 1]  == "b") & (plateau[ligne_arriver - 2][colonne_arriver + 2]  == " ")) | ((plateau[ligne_arriver + 1][colonne_arriver - 1]  == "b") & (plateau[ligne_arriver + 2][colonne_arriver - 2]  == " ")) | ((plateau[ligne_arriver + 1][colonne_arriver + 1]  == "b") & (plateau[ligne_arriver + 2][colonne_arriver + 2]  == " ")) ):
        #relance la fct DeplacerPionNoir avec la position d'arriver du pion Noir
        DeplacerPionNoir(lettre_arriver,chiffre_arriver,position_arriver)

def Dame():
    for case in range(1,12):
        #permet de changer les pions blancs en dame lorsqu'il arrive sur la derniere ligne des cases noirs
        if plateau[2][case] == "b":
            # "r" définie les dames blanches
            plateau[2][case] = "r"
            #permet de changer les pions blancs en dame lorsqu'il arrive sur la derniere ligne des cases noirs
        if plateau[11][case] == "n":
            # "r" définie les dames blanches
            plateau[11][case] = "l"

def TrouverDameBlanc():
    dames_blancs_jouables = []

    for ligne in range(2,12):

        for case in range(1,12):

            if plateau[ligne][case] == "r":
                dames_blancs_jouables.append(str(plateau[0][case] + plateau[ligne][12]))

    if dames_blancs_jouables != []:
        print("Le joueur BLANC peut bouger les dames sur les cases : " + str(dames_blancs_jouables))
        return dames_blancs_jouables

def TrouverDameNoir():
    dames_noirs_jouables = []

    for ligne in range(2,12):

        for case in range(1,12):

            if plateau[ligne][case] == "l":
                dames_noirs_jouables.append(str(plateau[0][case] + plateau[ligne][12]))

    if dames_noirs_jouables != []:
        print("Le joueur NOIR peut bouger les dames sur les cases : " + str(dames_noirs_jouables))
        return dames_noirs_jouables
                
def DeplacementDame(ligne_depart,colonne_depart,ligne_arriver,colonne_arriver):
    for i in range(1,9):
        if ( (ligne_arriver == (ligne_depart - i)) & (colonne_arriver == (colonne_depart - i)) ) | ( (ligne_arriver == (ligne_depart - i)) & (colonne_arriver == (colonne_depart + i)) ) | ( (ligne_arriver == (ligne_depart + i)) & (colonne_arriver == (colonne_depart - i)) ) | ( (ligne_arriver == (ligne_depart + i)) & (colonne_arriver == (colonne_depart + i)) ):
                
            #permettre de redemander une autre position si les casse en diagonales du pion choisi ne pas vide
            if (plateau[ligne_depart][colonne_depart] == "r") & (plateau[ligne_arriver][colonne_arriver] == " "):
                plateau[ligne_arriver][colonne_arriver] = "r"
                plateau[ligne_depart][colonne_depart] = " "

                AffichePlateau()

                break

            elif (plateau[ligne_depart][colonne_depart] == "l") & (plateau[ligne_arriver][colonne_arriver] == " "):
                plateau[ligne_arriver][colonne_arriver] = "l"
                plateau[ligne_depart][colonne_depart] = " "

                AffichePlateau()

                break  
    
def Start():
    os.system('cls' if os.name == 'nt' else 'clear')
    pion_noir = 20
    pion_blanc = 20

    print("Lancer la partie.")
    partie = input("Taper (ALEATOIRE/NORMAL)  ")

    if partie == "ALEATOIRE":
        PlateauAlaatoire(pion_noir,pion_blanc)

    elif partie == "NORMAL":
        InitialisationDesPions()

    else:
        Start() 

    print(end="\n")
    print("Voici le tableau des scores des Joueurs")
    print(end="\n")

    AfficheScore(pion_noir,pion_blanc)
    print(end="\n")
    print("La partie va bientot commencer !!!")
    PartieBlanc(pion_noir,pion_blanc)

def main():
    Start()

if __name__ == "__main__":
    main()
    